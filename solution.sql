CREATE DATABASE blog_db;
USE blog_db;

CREATE TABLE users (
    id INT NOT NUll AUTO_INCREMENT,
    email VARCHAR(100) NOT NUll,
    password VARCHAR(300) NOT NUll,
    datetime_created DATETIME NOT NUll,
    PRIMARY KEY (id)
);

CREATE TABLE posts (
    id INT NOT NUll AUTO_INCREMENT,
    author_id INT NOT NUll,
    title VARCHAR(500) NOT NUll,
    content VARCHAR(5000) NOT NUll,
    datetime_posted DATETIME NOT NUll,
    PRIMARY KEY (id),
    CONSTRAINT fk_posts_users_id
        FOREIGN KEY (author_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

CREATE TABLE post_comments (
    id INT NOT NUll AUTO_INCREMENT,
    post_id INT NOT NUll,
    user_id INT NOT NUll,
    content VARCHAR(5000),
    datetime_commented DATETIME NOT NUll,
    PRIMARY KEY (id),
    CONSTRAINT post_comments_posts_id
        FOREIGN KEY (post_id) REFERENCES posts(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT post_comments_users_id
        FOREIGN KEY (user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);


CREATE TABLE post_likes (
    id INT NOT NUll AUTO_INCREMENT,
    post_id INT NOT NUll,
    user_id INT NOT NUll,
    datetime_liked DATETIME NOT NUll,
    PRIMARY KEY (id),
    CONSTRAINT post_likes_posts_id
        FOREIGN KEY (post_id) REFERENCES posts(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT post_likes_users_id
        FOREIGN KEY (user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);